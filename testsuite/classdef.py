class Test:
    def __init__(self):
        return

    def func1(self):
        return


class TestSecond:
    def __init__(self):
        return

    def func2(self):
        return

    class TestThird(Test):
        def __init__(self):
            Test.__init__(self)

        def func3(self):
            return
