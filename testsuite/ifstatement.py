def test():
    return 2


if test() == 2:
    a = 2

if test() == 2:
    a = 2
else:
    a = 1

if test() == 2:
    a = 2
elif test() == 1:
    a = 1
else:
    a = 3

if test() == 2:
    a = 2
elif test() == 3:
    a = 1
elif test() == 3:
    a = 1
else:
    a = 3
