current_id = 0


class Location:
    def __init__(self, line, column, file):
        self.line = line
        self.column = column + 1
        self.file = file

    def get_line(self):
        return self.line

    def get_column(self):
        return self.column

    def get_file(self):
        return self.file


declarations_array = []


def get_declarations_array():
    global declarations_array
    return declarations_array


class AbstractDeclaration:
    def __init__(self, name, location):
        global current_id
        global declarations_array
        declarations_array.append(self)
        current_id += 1
        self.id = current_id
        self.name = name
        self.location = location
        self.inner_objects = []
        self.inner_classes = []
        self.inner_functions = []
        self.sensor = None

    def get_name(self):
        return self.name

    def get_location(self):
        return self.location

    def get_id(self):
        return self.id


callable_functions_dict = dict()


class FunctionDeclaration(AbstractDeclaration):
    def __init__(self, name, function_type, location):
        AbstractDeclaration.__init__(self, name, location)
        global callable_functions_dict
        if not name in callable_functions_dict:
            callable_functions_dict[name] = []
        callable_functions_dict[name].append(self.id)
        self.function_type = function_type
        self.entry_statement = None

    def get_inner_functions(self):
        return self.inner_functions

    def get_entry_statement(self):
        return self.entry_statement

    def set_entry_statement(self, statement):
        self.entry_statement = statement

    def get_inner_classes(self):
        return self.inner_classes


class ClassDeclaration(AbstractDeclaration):
    def __init__(self, name, location):
        AbstractDeclaration.__init__(self, name, location)

    def get_inner_classes(self):
        return self.inner_classes

    def get_inner_functions(self):
        return self.inner_functions


class ModuleDeclaration(ClassDeclaration):
    def __init__(self, location):
        ClassDeclaration.__init__(self, location.get_file(), location)
        self.entry_statement = None

    def get_entry_statement(self):
        return self.entry_statement

    def set_entry_statement(self, statement):
        self.entry_statement = statement


statements_array = []


def get_statements_array():
    global statements_array
    return statements_array


class AbstractStatement:
    def __init__(self, location):
        global statements_array
        statements_array.append(self)
        self.location = location
        global current_id
        current_id += 1
        self.id = current_id
        self.next_statement = None
        self.sensor = None

    def get_location(self):
        return self.location

    def get_next_statement(self):
        return self.next_statement

    def set_next_statement(self, statement):
        self.next_statement = statement

    def get_id(self):
        return self.id

    def set_id(self, id):
        self.id = id

    def set_sensor(self, sensor):
        self.sensor = sensor

    def get_sensor(self):
        return self.sensor

operations_array = []


def get_operations_array():
    global operations_array
    return operations_array


class Operation:
    def __init__(self):
        self.referenced_names = []
        global current_id
        current_id += 1
        self.id = current_id
        operations_array.append(self)

    def get_id(self):
        return self.id

    def get_referenced_names(self):
        return self.referenced_names

    def get_referenced_functions(self):
        if len(self.referenced_names) == 0:
            return []

        global callable_functions_dict

        result = []
        for name in  self.referenced_names:
            if name in callable_functions_dict:
                for id in callable_functions_dict[name]:
                    result.append(id)

        return result


class OperationalStatement(AbstractStatement):
    def __init__(self, location):
        AbstractStatement.__init__(self, location)
        self.operation = Operation()

    def get_operation(self):
        return self.operation


class EntryStatement(AbstractStatement):
    def __init__(self, location):
        AbstractStatement.__init__(self, location)


class PairOperationStatement:
    def __init__(self, location, operation, statement):
        self.location = location
        self.operation = operation
        self.statement = statement

    def get_location(self):
        return self.location

    def get_operation(self):
        return self.operation

    def get_statement(self):
        return self.statement


class ElseBasedStatements(AbstractStatement):
    def __init__(self, location):
        AbstractStatement.__init__(self, location)
        self.else_statement = None

    def set_else_statement(self, statement):
        self.else_statement = statement

    def get_else_statement(self):
        return self.else_statement


class OperationStatements:
    def __init__(self):
        self.operation = Operation()
        self.body = None

    def get_operation(self):
        return self.operation

    def set_body(self, statement):
        self.body = statement

    def get_body(self):
        return self.body


class IfStatement(ElseBasedStatements, OperationStatements):  # #7.1
    def __init__(self, location):
        ElseBasedStatements.__init__(self, location)
        OperationStatements.__init__(self)


class ForStatement(ElseBasedStatements, OperationStatements):  # # 7.3
    def __init__(self, location):
        ElseBasedStatements.__init__(self, location)
        OperationStatements.__init__(self)


class WhileStatement(ElseBasedStatements, OperationStatements):  # # 7.2
    def __init__(self, location):
        ElseBasedStatements.__init__(self, location)
        OperationStatements.__init__(self)


class TryStatement(ElseBasedStatements, OperationStatements):  # # 7.4
    def __init__(self, location):
        ElseBasedStatements.__init__(self, location)
        OperationStatements.__init__(self)
        self.except_statements = []
        self.finally_statement = None

    def get_except_statements(self):
        return self.except_statements

    def get_finally_statement(self):
        return self.finally_statement

    def set_finally_statement(self, statement):
        self.finally_statement = statement


class WithStatement(AbstractStatement, OperationStatements):  # # 7.5 FIXME
    def __init__(self, location):
        AbstractStatement.__init__(self, location)
        OperationStatements.__init__(self)


class BreakStatement(OperationalStatement):
    def __init__(self, location, statement_type):
        OperationalStatement.__init__(self, location)
        self.statement_type = statement_type
        self.breaking_loop = None

    def get_statement_type(self):
        return self.statement_type

    def set_breaking_loop(self, value):
        self.breaking_loop = value

    def get_breaking_loop(self):
        return self.breaking_loop

