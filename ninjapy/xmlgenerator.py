from ast_structure import *
import kajiki

file_array = None


def generate_xml(file_arr, file_name):
    global file_array
    file_array = file_arr
    declarations_arrays = get_declarations_array()
    statements_arrays = get_statements_array()
    operations_arrays = get_operations_array()

    output_file = open(file_name, 'w')
    output_file.write(u'''<Database>
  <Files>''')

    print 'Files'
    for i, val in enumerate(file_array):
        output_file.write("\n" + generate_one_file(val, i + 1))

    output_file.write(u'''  </Files>
  <Classes>''')

    print 'Classes'
    for val in declarations_arrays:
        if isinstance(val, ModuleDeclaration):
            continue
        if isinstance(val, ClassDeclaration):
            output_file.write("\n" + generate_one_class(val))

    output_file.write(u'''  </Classes>
  <Variables>
  </Variables>
  <Functions>''')
    print 'Functions'
    for val in declarations_arrays:
        if isinstance(val, FunctionDeclaration):
            output_file.write("\n" + generate_one_function(val))
        elif isinstance(val, ModuleDeclaration):
            output_file.write("\n" + generate_one_function(val))

    output_file.write(u'''  </Functions>
  <Statements>''')

    print 'Statements'
    for statement in statements_arrays:
        xml_statement = generate_one_statement(statement)
        if xml_statement is not None:
            output_file.write("\n" + xml_statement)

    output_file.write(u'''  </Statements>
  <Operations>''')
    print 'Operations'
    for operation in operations_arrays:
        xml_operation = generate_one_operation(operation)
        output_file.write("\n" + xml_operation)
    output_file.write(u'''  </Operations>
  </Database>''')
    output_file.close()
    return


def generate_one_file(file_info, file_id):
    kij_file = kajiki.XMLTemplate(u'''
    <File fullname="$name" ID="$id" />''')
    return kij_file(dict(name=file_info, id=file_id)).render()


def generate_one_class(class_info):
    kij_class = kajiki.XMLTemplate(u'''
    <Class ID="$id" Name="$name">
      <MethodId py:for="x in method_ids">$x</MethodId>
    </Class>''')

    method_ids = list(map(lambda x: x.get_id(),  class_info.get_inner_functions()))
    return kij_class(dict(name=class_info.get_name(), id=class_info.get_id(), method_ids=method_ids)).render()


def generate_one_function(function_info):
    global file_array
    kij_function = kajiki.XMLTemplate(u'''
    <Function ID="$id" Name="$name">
      <Definition>
        <LocationLine line="$line" column="$column" file="$file_id"/>
      </Definition>
      <py:if test="entry_id is not None">
      <EntryStatement ID="$entry_id" kind="$entry_kind" />
      </py:if>
    </Function>''')

    entry = generate_statement_reference(function_info.get_entry_statement())

    return kij_function(dict(
        name=function_info.get_name(),
        id=function_info.get_id(),
        line=function_info.get_location().get_line(),
        column=function_info.get_location().get_column(),
        file_id=file_array.index(function_info.get_location().get_file()) + 1,
        entry_id=entry.id,
        entry_kind=entry.kind
    )).render()


def generate_one_statement(statement):
    global file_array
    if statement.get_id() in met_ids:
        return None

    if isinstance(statement, EntryStatement):
        statement = statement.get_next_statement()
        if statement is not None:
            met_ids.append(statement.get_id())

    if statement is None:
        return None

    next_statement = generate_statement_reference(statement.get_next_statement())

    info = dict(
        id=statement.get_id(),
        line=statement.get_location().get_line(),
        column=statement.get_location().get_column(),
        file_id=file_array.index(statement.get_location().get_file()) + 1,
        sensor=statement.get_sensor() or None,
        next_id=next_statement.id,
        next_kind=next_statement.kind
    )

    if isinstance(statement, BreakStatement):
        br_type = statement.get_statement_type()
        if br_type == 'raise':
            info['exception'] = statement.get_operation().get_id()
            return generate_throw(info).render()
        elif br_type == 'break':
            breaking_loop = generate_statement_reference(statement.get_breaking_loop())
            info['break_id'] = breaking_loop.id
            info['break_kind'] = breaking_loop.kind
            return generate_break(info).render()
        elif br_type == 'continue':
            continuing_loop = generate_statement_reference(statement.get_breaking_loop())
            info['continue_id'] = continuing_loop.id
            info['continue_kind'] = continuing_loop.kind
            return generate_continue(info).render()
        else:
            info['return_id'] = statement.get_operation().get_id()
            return generate_return(info).render()
    elif isinstance(statement, OperationalStatement):
        info['operation_id'] = statement.get_operation().get_id()
        return generate_operational(info).render()
    elif isinstance(statement, TryStatement):
        info['try_block'] = generate_statement_reference(statement.get_body())
        info['catch_blocks'] = []
        for catch in statement.get_except_statements():
            catch_block = generate_statement_reference(catch)
            if catch_block is not None:
                info['catch_blocks'].append(catch_block)
        info['else_block'] = None
        if statement.get_else_statement() is not None:
            info['else_block'] = generate_statement_reference(statement.get_else_statement())
        info['finally_block'] = None
        if statement.get_finally_statement() is not None:
            info['finally_block'] = generate_statement_reference(statement.get_finally_statement())
        return generate_try(info).render().replace('<r>','').replace('</r>','')
    elif isinstance(statement, IfStatement):
        info['then_'] = generate_statement_reference(statement.get_body())
        info['condition'] = statement.get_operation().get_id()
        info['else_'] = None
        if statement.get_else_statement() is not None:
            info['else_'] = generate_statement_reference(statement.get_else_statement())
        return generate_if(info).render()
    elif isinstance(statement, WhileStatement):
        info['isCheckConditionBeforeFirstRun'] = True
        info['body'] = generate_statement_reference(statement.get_body())
        info['condition'] = statement.get_operation().get_id()
        info['else_block'] = None
        if statement.get_else_statement() is not None:
            info['else_block'] = generate_statement_reference(statement.get_else_statement())
            info['else_block'].isOnBreak = False
        return generate_dowhile(info).render()
    elif isinstance(statement, ForStatement):
        info['body'] = generate_statement_reference(statement.get_body())
        info['head'] = statement.get_operation().get_id()
        return generate_for(info).render()
    return None


def generate_break(info):
    kij_st = kajiki.XMLTemplate(u'''
    <STBreak ID="$id">
      <py:if test="next_id is not None">
      <NextInLinearBlock ID="$next_id" kind="$next_kind" />
      </py:if>
      <py:if test="sensor is not None">
      <SensorBeforeTheStatement>$sensor</SensorBeforeTheStatement>
      </py:if>
      <FirstSymbolLocation>
        <LocationLine line="$line" column="$column" file="$file_id"/>
      </FirstSymbolLocation>
      <LastSymbolLocation />
      <breakingLoop ID="$break_id" kind="$break_kind" />
    </STBreak>''')
    return kij_st(info)


def generate_throw(info):
    kij_st = kajiki.XMLTemplate(u'''
    <STThrow exception="$exception" ID="$id">
      <py:if test="next_id is not None">
      <NextInLinearBlock ID="$next_id" kind="$next_kind" />
      </py:if>
      <py:if test="sensor is not None">
      <SensorBeforeTheStatement>$sensor</SensorBeforeTheStatement>
      </py:if>
      <FirstSymbolLocation>
        <LocationLine line="$line" column="$column" file="$file_id"/>
      </FirstSymbolLocation>
      <LastSymbolLocation />
    </STThrow>''')
    return kij_st(info)


def generate_continue(info):
    kij_st = kajiki.XMLTemplate(u'''
    <STContinue ID="$id">
      <py:if test="next_id is not None">
      <NextInLinearBlock ID="$next_id" kind="$next_kind" />
      </py:if>
      <py:if test="sensor is not None">
      <SensorBeforeTheStatement>$sensor</SensorBeforeTheStatement>
      </py:if>
      <FirstSymbolLocation>
        <LocationLine line="$line" column="$column" file="$file_id"/>
      </FirstSymbolLocation>
      <LastSymbolLocation />
      <continuingLoop ID="$continue_id" kind="$continue_kind" />
    </STContinue>''')
    return kij_st(info)


def generate_return(info):
    kij_st = kajiki.XMLTemplate(u'''
    <STReturn ReturnOperation="$return_id" ID="$id">
      <py:if test="next_id is not None">
      <NextInLinearBlock ID="$next_id" kind="$next_kind" />
      </py:if>
      <py:if test="sensor is not None">
      <SensorBeforeTheStatement>$sensor</SensorBeforeTheStatement>
      </py:if>
      <FirstSymbolLocation>
        <LocationLine line="$line" column="$column" file="$file_id"/>
      </FirstSymbolLocation>
      <LastSymbolLocation />
    </STReturn>''')
    return kij_st(info)


def generate_operational(info):
    kij_st = kajiki.XMLTemplate(u'''
    <STOperational operation="$operation_id" ID="$id">
      <py:if test="next_id is not None">
      <NextInLinearBlock ID="$next_id" kind="$next_kind" />
      </py:if>
      <py:if test="sensor is not None">
      <SensorBeforeTheStatement>$sensor</SensorBeforeTheStatement>
      </py:if>
      <FirstSymbolLocation>
        <LocationLine line="$line" column="$column" file="$file_id"/>
      </FirstSymbolLocation>
      <LastSymbolLocation />
    </STOperational>
    ''')
    return kij_st(info)


def generate_try(info):
    kij_st = kajiki.XMLTemplate(u'''
    <STTryCatchFinally ID="$id">
          <py:if test="next_id is not None">
          <NextInLinearBlock ID="$next_id" kind="$next_kind" />
          </py:if>
          <py:if test="sensor is not None">
          <SensorBeforeTheStatement>$sensor</SensorBeforeTheStatement>
          </py:if>
          <FirstSymbolLocation>
            <LocationLine line="$line" column="$column" file="$file_id"/>
          </FirstSymbolLocation>
          <LastSymbolLocation />
          <tryBlock ID="$try_block.id" kind="$try_block.kind" />
          <r py:for="x in catch_blocks">
            <catchBlock ID="$x.id" kind="$x.kind" />
          </r>
          <py:if test="else_block is not None">
          <elseBlock ID="$else_block.id" kind="$else_block.kind" />
          </py:if>
          <py:if test="finally_block is not None">
          <finallyBlock ID="$finally_block.id" kind="$finally_block.kind" />
          </py:if>
        </STTryCatchFinally>''')
    return kij_st(info)


def generate_dowhile(info):
    kij_st = kajiki.XMLTemplate(u'''
    <STDoAndWhile condition="$condition" isCheckConditionBeforeFirstRun="1" ID="$id">
      <py:if test="next_id is not None">
      <NextInLinearBlock ID="$next_id" kind="$next_kind" />
      </py:if>
      <py:if test="sensor is not None">
      <SensorBeforeTheStatement>$sensor</SensorBeforeTheStatement>
      </py:if>
      <FirstSymbolLocation>
      <LocationLine line="$line" column="$column" file="$file_id"/>
      </FirstSymbolLocation>
      <LastSymbolLocation />
      <body ID="$body.id" kind="$body.kind" />
      <py:if test="else_block is not None">
      <else isOnBreak="OnlyOnBreak" ID="$else_block.id" kind="$else_block.kind" />
      </py:if>
    </STDoAndWhile>''')
    return kij_st(info)


def generate_if(info):
    kij_st = kajiki.XMLTemplate(u'''
    <STIf condition="$condition" ID="$id">
      <py:if test="next_id is not None">
      <NextInLinearBlock ID="$next_id" kind="$next_kind" />
      </py:if>
      <py:if test="sensor is not None">
      <SensorBeforeTheStatement>$sensor</SensorBeforeTheStatement>
      </py:if>
      <FirstSymbolLocation>
      <LocationLine line="$line" column="$column" file="$file_id"/>
      </FirstSymbolLocation>
      <LastSymbolLocation />
      <then ID="$then_.id" kind="$then_.kind" />
      <py:if test="else_ is not None">
      <else ID="$else_.id" kind="$else_.kind" />
      </py:if>
    </STIf>''')
    return kij_st(info)


def generate_for(info):
    kij_st = kajiki.XMLTemplate(u'''
    <STForEach head="$head" ID="$id">
      <py:if test="next_id is not None">
      <NextInLinearBlock ID="$next_id" kind="$next_kind" />
      </py:if>
      <py:if test="sensor is not None">
      <SensorBeforeTheStatement>$sensor</SensorBeforeTheStatement>
      </py:if>
      <FirstSymbolLocation>
      <LocationLine line="$line" column="$column" file="$file_id"/>
      </FirstSymbolLocation>
      <LastSymbolLocation />
      <body ID="$body.id" kind="$body.kind" />
    </STForEach>''')
    return kij_st(info)


def generate_one_operation(operation):
    kij_st = kajiki.XMLTemplate(u'''
    <Operation ID="$id">
        <callNode  py:for="x in function_ids">
            <callFunction ID="$x" kind="function"/>
        </callNode>
    </Operation>''')

    operation_info=dict(id=operation.get_id(), function_ids=operation.get_referenced_functions())
    return kij_st(operation_info).render()


met_ids = []


def generate_statement_reference(next_statement):
    ret = type('',(object,),{"id": None, "kind": None})()
    if next_statement is None:
        return ret

    if isinstance(next_statement, EntryStatement):
        next_statement = next_statement.get_next_statement()

    if next_statement is None:
        return ret

    if next_statement is not None:
        ret.id = next_statement.get_id()
        ret.kind = get_xmlstatement_name(next_statement)
        return ret


def get_xmlstatement_name(statement):
    if isinstance(statement, BreakStatement):
        br_type = statement.get_statement_type()
        if br_type == 'raise':
            return 'STThrow'
        elif br_type == 'break':
            return 'STBreak'
        elif br_type == 'continue':
            return 'STContinue'
        else:
            return 'STReturn'
    elif isinstance(statement, OperationalStatement):
        return 'STOperational'
    elif isinstance(statement, TryStatement):
        return 'STTryCatchFinally'
    elif isinstance(statement, IfStatement):
        return 'STIf'
    elif isinstance(statement, WhileStatement):
        return 'STDoAndWhile'
    elif isinstance(statement, ForStatement):
        return 'STForEach'
