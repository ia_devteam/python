from setuptools import setup

setup(name='ninjapy',
      version='0.1',
      description='Python parser',
      url='http://github.com/storborg/funniest',
      author='Ninja Coder',
      author_email='dimka87@gmail.com',
      license='MIT',
      packages=['ninjapy'],
      install_requires=[
          'kajiki',
          'astor'
      ],
      zip_safe=False)